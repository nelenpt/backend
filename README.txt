1. Describe possible performance optimizations for your Code.
Overall this exercise was quite easy and straight forward. My motto is to not query the backend unless it's necessary and that's why I've chosen to store everything in the browser's Local Storage instead of creating sessions and storing them server side. That would create unnecessary overhead for a rather simple exercise.
So the whole backend and frontend only do 2 API calls to the backend instead of many (if storing everything in the backend), but there may be cases where partial data might be useful and although it's not performant, it's the best solution.

2. Which things could be done better, than you’ve done it?
I've been constraint to time and it being the weekend. Weekends are always complicated because of having my daughter around and always wanting to play and go out, so I had to make conscious decisions for the sake of rapidity:
- The frontend is essentially just one big file, I'm not using proper separation of code into components or the proper use of Vue's store.
- Certain settings that could have been environment/global variables were hardcoded instead of being added into a .env file and are hardcoded into the code itself (API endpoints for example).
- I didn't have the time to set up a proper database online so the paymentDataIds are not being stored, although I did write their migration files.
- I generally write unit tests, but then again, due to time constraints, decided to skip those.
- Validation could be better, but does it's job of not allowing the user to finalise the process without all the necessary information.
- I generally always use transformers for API responses, but skipped it for this exercise because there's only 1 endpoint and it's not supported out of the box by Laravel.

Otherwise, everything else works fine:
- Leaving and coming back the webpage will take the user to the first available step that doesn't have all the required information filled.
- Finishing a step will mark the step as complete and the user will have the visual feedback of a check mark on the stepper.
- If the API returns an error, the user is taken back to step 3 to retry.
- The user can click on any of the steps to be taken to that step.