<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Http;
use Illuminate\Http\Request;
use App\Http\Requests\PaymentRequest;

class PaymentController extends Controller
{
    
    /**
     * Main method of the whole API
     * 
     * @bodyParam owner string required Account holder name
     * @bodyParam iban string required Account number
     */
    public function create(PaymentRequest $request)
    {
        if ($call = $this->submitDataWunderBackend($request->get('iban'), $request->get('owner'))) {
            return response()->json(['paymentCode' => $call]);
            // TODO: Don't have an easy way to deploy a database to save the request's info
        }
        return response()->json(['message' => 'An error has occured'], 400);
    }

    /**
     * Submits the payment information through Wunder Mobility's API 
     */
    private function submitDataWunderBackend($iban, $owner)
    {
        $data = ['customerId' => 1, 'iban' => $iban, 'owner' => $owner];
        $request = Http::post('https://37f32cl571.execute-api.eu-central-1.amazonaws.com/default/wunderfleet-recruiting-backend-dev-save-payment-data', $data);
        if (isset($request->json()['paymentDataId'])) {
            return $request->json()['paymentDataId'];
        }
        return false;
    }

}
