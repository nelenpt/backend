<?php

namespace App\Exceptions;

use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Validation\ValidationException;
use Throwable;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {
        $this->renderable(function (Exception $e, $request) {
            $this->render($request, $e);
        });
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Throwable  $exception
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @throws \Throwable
     */
    public function render($request, Throwable $exception)
    {
        if ($exception instanceof AuthenticationException || $exception instanceof AuthorizationException) {
            return response()->json(['message' => 'Unauthorized.'], 401);
        } elseif ($exception instanceof ValidationException) {
            return response()->json(['error' => true, 'message' => 'Validation error.', 'errors' => $exception->errors()], 400);
        }

        // Prevents the API to return Laravel's default HTML error messages and instead shows a simple JSON with the right status code.
        elseif ($this->isHttpException($exception) || $exception instanceof ModelNotFoundException) {
            return response()->json(['message' => 'Not found.'], 404);
        }

        return parent::render($request, $exception);
    }

}
